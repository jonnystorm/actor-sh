# actor.sh

LOL Actor(i)sh messaging for BASH scripts

Seriously: why would you ever want to do such a horrible
thing?

Still, if that's what you want, you'll need
[`socat`](http://www.dest-unreach.org/socat/).


## Demo

Start the server:

```shell
[22:09:13] jstorm[0]@alarbus:actor-sh$ ./server.sh 0
Init process 20365...
```

Run the poller:

```shell
[22:09:13] jstorm[0]@alarbus:actor-sh$ ./poller.sh 20365
Init process 20553...
Server status: Just groovy!
Server death rattle: Noooooes!
Terminating...
[22:11:18] jstorm[0]@alarbus:actor-sh$
```

Meanwhile, at the server:

```shell
[22:09:13] jstorm[0]@alarbus:actor-sh$ ./server.sh 0
Init process 20365...
Terminating...
[22:11:18] jstorm[0]@alarbus:actor-sh$
```

