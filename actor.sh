# Copyright © 2018 Jonathan Storm <jds@idio.link>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

function send
{
  declare -r to=$1
  shift
  declare -r msg=$@

  echo "$msg" |
    socat STDIN ABSTRACT-SENDTO:$to
}

function receive
{
  declare -r after=${1:-1}
  declare     line=""

  case $after in
    0)
      read line <&500
      echo "$line"
      ;;
    [1-9]*)
      read -t $after line <&500
      echo "$line"
      ;;
    *)
      echo "[ERROR] Invalid receive timeout: '$after'" >&2
      exit 1
  esac
}

function __init
{
  echo "Init process $$..." >&2

  exec 500< \
    <(socat ABSTRACT-RECV:$$ STDOUT 2>/dev/null)
}

function __terminate
{
  echo "Terminating..." >&2

  exec 500<&-
}

trap __terminate EXIT

__init

