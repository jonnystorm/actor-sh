#!/bin/bash
#
# Copyright © 2018 Jonathan Storm <jds@idio.link>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

ROOT=$(dirname $0)

source "$ROOT"/sane.sh
source "$ROOT"/actor.sh


function main
{
  var server_pid=$1

  send $server_pid $$ stat
  echo "Server status: $(receive)"

  send $server_pid $$ die
  echo "Server death rattle: $(receive)"

  exit 0
}


if [[ $# -ne 1 ]] ||
   ! $(ps -p $1 &>/dev/null)
then
  echo "Usage: $0 <pid_of_running_server>" >&2
  exit 1
fi

main $@

