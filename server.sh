#!/bin/bash
#
# Copyright © 2018 Jonathan Storm <jds@idio.link>
# This work is free. You can redistribute it and/or modify it under the
# terms of the Do What The Fuck You Want To Public License, Version 2,
# as published by Sam Hocevar. See the COPYING.WTFPL file for more details.

ROOT=$(dirname $0)

source "$ROOT"/sane.sh
source "$ROOT"/actor.sh


function dispatch
{
  var from=$1
  var   op=$2
  shift; shift
  var  msg=$@

  case $op in
    stat) send $from "Just groovy!";;
     die) send $from "Noooooes!" && exit 0;;
       *) echo "[INFO] Got unexpected message: '$msg'" >&2;;
  esac
}

function main
{
  var timeout=${1:-0}

  mut msg=""
  while true
  do
    msg="$(receive $timeout)"

    [ $timeout -gt 0 ] &&
      echo "[DEBUG] Receive timed out" >&2

    [ -n "$msg" ] && dispatch $msg
  done
}


if [[ $# -eq 1 ]] &&
   [[ ! $1 =~ ^[0-9]+ ]]
then
  echo "Usage: $0 [<receive_timeout_s>]" >&2
  exit 1
fi

main $@

